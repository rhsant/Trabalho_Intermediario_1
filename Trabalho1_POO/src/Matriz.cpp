#include "Matriz.h"

#include <iostream>

Matriz::Matriz(): Matriz(1,1){
    //construtor que chama o construtor parametrizado com dimensoes 1x1.
}

Matriz::~Matriz(){
    //cout<<"\nInside Destructor"<<endl;
    //Reduzir o vetor elementos aos tamanhos (0,0)
    elements.resize(0);
    elements[0].resize(0);
    //cout<<getRows()<<" "<<getCols()<<endl;
    //dtor
}

//Inicizaliza alocando espa�o para a matriz
Matriz::Matriz(int x, int y){
    //aloca espa�o para o vetor externo
    elements.resize(x);
        for (int line = 0; line < x; line++){
            //aloca espa�o para o vetor interno
            elements[line].resize(y);
        }
    //Checando se alocou corretamente.
    //std::cout<<elements.size()<<" "<<elements[0].size()<<std::endl;
}

//Inicializa matriz com zeros
void Matriz::zeros(){
        for (int line = 0; line < getRows(); line++){
                for(int collumn = 0; collumn < getCols(); collumn++){
                elements[line][collumn] = 0.0;
            }
        }
}

void Matriz::ones(){
        for (int line = 0; line < getRows(); line++){
                for(int collumn = 0; collumn < getCols(); collumn++){
                elements[line][collumn] = 1.0;
            }
        }
}

int Matriz::getRows() const{
    //retorna altura da matriz (numero de linhas)
    return elements.size();
}

int Matriz::getCols() const{
    //retorna largura da matriz (numero de colunas)
    return elements[0].size();
}

//Inicializa como matriz identidade
void Matriz::unit(){
    //checa se a matriz � quadrada
    if(getCols() == getRows()){

        for (int line = 0; line < getRows(); line++){
            for(int collumn = 0; collumn < getCols(); collumn++){
                //se os indices sao iguais, coloca 1, se diferentes, coloca 0
                if (line == collumn) elements[line][collumn] = 1.0;
                else elements[line][collumn] = 0.0;
            }
        }
    }
    else        std::cout<<"Matriz nao quadrada!"<<std::endl;
}

//Fun��o print que imprime a matriz.
void Matriz::print() const {
        for (int line = 0; line < getRows(); line++){
            for(int collumn = 0; collumn < getCols(); collumn++){
                //se os indices sao iguais, coloca 1, se diferentes, coloca 0
                std::cout<<elements[line][collumn]<<" ";
            }
            std::cout<<std::endl;
        }
}

//SOBRECARGAS DE OPERADORES

//Sobrescreve o operador << de cout para a classe!
std::ostream& operator<<(std::ostream& stream, const Matriz& matriz){
    for (int line = 0; line < matriz.getRows(); line++){
            for(int collumn = 0; collumn < matriz.getCols(); collumn++){
                //se os indices sao iguais, coloca 1, se diferentes, coloca 0
                stream<<matriz.elements[line][collumn]<<" ";
            }
            stream<<std::endl;
        }
        return stream;
 }


  //Sobrescreve o operador () para retornar a refer�ncia para a posi��o que se deseja alterar;
 //N�o foi definida como const devido ao erro de compila��o "binding 'const double' to reference of type 'double&' discards qualifiers"
double &Matriz::operator()(int x, int y) {
     double &position = this->elements[x-1][y-1];
     //std::cout<<position<<std::endl;
     return position;
}

//Retorno por valor!
Matriz Matriz::operator +(Matriz& mat) const{
    Matriz result (this->getRows(), this->getCols());
    //Se matrizes tem a mesma dimens�o
    if (this->getCols() == mat.getCols() && this->getRows() == mat.getRows()){
        for (int line = 0; line < mat.getRows(); line++){
            for(int collumn = 0; collumn < mat.getCols(); collumn++){
                result.elements[line][collumn] = this->elements[line][collumn] + mat.elements[line][collumn];
            }
        }
    }
    else{
        std::cout<<"Matrizes de tamanhos diferentes, resultado definido como matriz de zeros."<<std::endl;
    }
    return result;
}

void Matriz::operator +=(Matriz& mat){
     if (this->getCols() == mat.getCols() && this->getRows() == mat.getRows()){
        for (int line = 0; line < mat.getRows(); line++){
            for(int collumn = 0; collumn < mat.getCols(); collumn++){
                double &position = this->elements[line][collumn];
                position = this->elements[line][collumn] + mat.elements[line][collumn];
                //std::cout<<result.elements[line][collumn]<<std::endl;
            }
        }
    }
    else{
        std::cout<<"Matrizes de tamanhos diferentes, nao e possivel efetuar a soma!"<<std::endl;
    }
}

Matriz Matriz::operator -(Matriz& mat) const{
    Matriz result (this->getRows(), this->getCols());
    //Se matrizes tem a mesma dimens�o
    if (this->getCols() == mat.getCols() && this->getRows() == mat.getRows()){
        for (int line = 0; line < mat.getRows(); line++){
            for(int collumn = 0; collumn < mat.getCols(); collumn++){
                result.elements[line][collumn] = this->elements[line][collumn] - mat.elements[line][collumn];
                //std::cout<<result.elements[line][collumn]<<std::endl;
            }
        }
    }
    else{
        std::cout<<"Matrizes de tamanhos diferentes, resultado definido como matriz de zeros"<<std::endl;
    }
    return result;
}

void Matriz::operator -=(Matriz& mat){
     if (this->getCols() == mat.getCols() && this->getRows() == mat.getRows()){
        for (int line = 0; line < mat.getRows(); line++){
            for(int collumn = 0; collumn < mat.getCols(); collumn++){
                double &position = this->elements[line][collumn];
                position = this->elements[line][collumn] - mat.elements[line][collumn];
                //std::cout<<result.elements[line][collumn]<<std::endl;
            }
        }
    }
    else{
        std::cout<<"Matrizes de tamanhos diferentes, nao e possivel efetuar a subtracao!"<<std::endl;
    }
}


Matriz Matriz::operator *(Matriz& mat) const{
    //MULTIPLICA��O DE MATRIZES
    //Matriz resultado com dimensoes de apropriadas
    Matriz result (this->getRows(), mat.getCols());

    if (this->getCols() == mat.getRows()){
        for (int line = 0; line < mat.getRows(); line++){
            for(int collumn = 0; collumn < mat.getCols(); collumn++){
               double aux = 0;
               for (int sumWalk = 0; sumWalk< this->getRows(); sumWalk++){
                aux += (this->elements[sumWalk][collumn] * mat.elements[line][sumWalk]);
               }
               result.elements[line][collumn] = aux;
            }
        }
    } else{
            std::cout<<"Matrizes de dimensoes inapropriadas. Nao e possivel efetuar a multiplicacao."<<std::endl;
    }
    return result;
}

Matriz Matriz::operator *(const double multp) const{
    //MULTIPLICA��O DE MATRIZES
    //Matriz resultado com dimensoes de apropriadas
    Matriz result (this->getRows(), this->getCols());

    for (int line = 0; line < this->getRows(); line++){
        for(int collumn = 0; collumn < this->getCols(); collumn++){
            result.elements[line][collumn] = multp * this->elements[line][collumn];
            //std::cout<<result.elements[line][collumn]<<std::endl;
        }
    }
    return result;
}

void Matriz::operator *=(const double multp) {
    for (int line = 0; line < this->getRows(); line++){
        for(int collumn = 0; collumn < this->getCols(); collumn++){
            double &position = this->elements[line][collumn];
            position = this->elements[line][collumn]*multp;
            //std::cout<<result.elements[line][collumn]<<std::endl;
        }
    }
}

void Matriz::operator *=(Matriz& mat){
    Matriz result (this->getRows(), mat.getCols());

    if (this->getCols() == mat.getRows()){
        for (int line = 0; line < mat.getRows(); line++){
            for(int collumn = 0; collumn < mat.getCols(); collumn++){
               double aux = 0;
               for (int sumWalk = 0; sumWalk< this->getRows(); sumWalk++){
                aux += (this->elements[sumWalk][collumn] * mat.elements[line][sumWalk]);
               }
               result.elements[line][collumn] = aux;
            }
        }
        (*this) = result;
    } else{
            std::cout<<"Matrizes de dimensoes inapropriadas. Nao e possivel efetuar a multiplicacao."<<std::endl;
    }
}

Matriz Matriz::operator -() const{
    Matriz result(this->getCols(), this->getRows());

    for (int line = 0; line < this->getRows(); line++) {
        for (int collumn = 0; collumn <this->getCols(); collumn++){
            result.elements[collumn][line] = this->elements[line][collumn];
        }
    }
    //(*this) = result;
    return result;
}

;
