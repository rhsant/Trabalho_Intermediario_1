#ifndef MATRIZ_H
#define MATRIZ_H

#include <vector>
#include <iostream>

class Matriz
{
    public:
        //construtor default
        Matriz();
        //construtor parametrizado
        //Aloca memoria para a matriz
        Matriz(int x, int y);
        //Inicializa A como matriz identidade
        //se a matriz for quadrada
        void unit();
        //Retorna numero de linhas (altura)
        int getRows() const;
        //Retorna numero de colunas (largura)
        int getCols() const;
        //Inicializa matriz com zeros
        void zeros();
        //Inicializa matriz com 1s
        void ones();
        //Imprime a matriz
        void print() const;

        //SOBRECARGAS DE OPERADORES

        //Sobrescreve o operador << de cout para a classe!
        friend std::ostream& operator<< (std::ostream& stream, const Matriz& );

        //N�o � const devido � referencia interna que � retornada
        double &operator()(int x, int y) ;

        Matriz operator +(Matriz& mat) const;
        //N�o � const devido � referencia
        void operator +=(Matriz& mat);

        Matriz operator -(Matriz& mat) const;
        //N�o � const devido � referencia
        void operator -=(Matriz& mat);
        //Multiplica��o de matrizes
        Matriz operator *(Matriz& mat) const;

        Matriz operator *(const double multp) const;
        //Multiplica��o de matriz e constante
        void operator *=(const double multp);

        void operator *=(Matriz& mat);
        //transposta
        Matriz operator -() const;

        virtual ~Matriz();

    protected:

    private:
        std::vector <std::vector<double> > elements;
};



#endif // MATRIZ_H
